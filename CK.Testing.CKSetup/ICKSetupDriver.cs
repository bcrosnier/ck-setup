using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Exposes an API to manipulate CKSetup.
    /// </summary>
    public interface ICKSetupDriver
    {
        /// <summary>
        /// Runs a configuration.
        /// </summary>
        /// <param name="configuration">The configuration to run. Must not be null.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="Facade.DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// The remote store url. Defaults to <see cref="Facade.DefaultStoreUrl"/>.
        /// Use "none" to explictely not sollicitating any remote store.
        /// </param>
        /// <returns>True on success, false if any error occurred.</returns>
        bool Run( SetupConfiguration configuration, string storePath = null, string remoteStoreUrl = null );

        /// <summary>
        /// Path to a folder for local stores: "<see cref="IBasicTestHelper.TestProjectFolder"/>/LocalStores".
        /// This folder is globally cleared once (the first time this helper is used).
        /// </summary>
        NormalizedPath LocalStoresFolder { get; }

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
        /// Use <see cref="GetCleanTestStorePath"/> to clear it before returning it.
        /// </summary>
        /// <param name="type">Type of the store.</param>
        /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to a zip file or a directory that may already exists.</returns>
        NormalizedPath GetTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null );

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
        /// If the store exists it is deleted.
        /// </summary>
        /// <param name="type">Type of the store.</param>
        /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to a zip file or directory that does not exist.</returns>
        NormalizedPath GetCleanTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null );

        /// <summary>
        /// Runs "dotnet publish" on an output bin folder.
        /// The configuration (Debug/Release) and target framework (netcoreapp2.0 for instance) are extracted
        /// from the path.
        /// </summary>
        /// <param name="pathToFramework">Path to the framework (typically ends with "bin/Debug/netcoreapp2.0").</param>
        /// <param name="force">
        /// True to force the publication. By default, if the "<paramref name="pathToFramework"/>/publish"
        /// folder exists, nothing is done.
        /// </param>
        /// <returns>The full path to the published folder.</returns>
        string DotNetPublish( string pathToFramework, bool force = false );

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core)
        /// into a local store.
        /// </summary>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="Facade.DefaultStorePath"/>.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        bool AddComponentFoldersToStore( IEnumerable<string> folders, string storePath = null );

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="storePath">The path to the local store to push.</param>
        /// <param name="remoteStoreUrl">The remote store url. Defaults to <see cref="Facade.DefaultStoreUrl"/>.</param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the remote store.</param>
        /// <returns>True on success, false on error.</returns>
        bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey );
    }
}
