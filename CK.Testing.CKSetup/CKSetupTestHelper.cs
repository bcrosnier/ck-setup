using CK.Core;
using CK.Testing.CKSetup;
using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing
{
    /// <summary>
    /// Exposes the <see cref="ICKSetupTestHelper"/> mixin test helper.
    /// </summary>
    public class CKSetupTestHelper : CKSetup.ICKSetupTestHelperCore
    {
        readonly ICKSetupDriver _driver;

        class Driver : ICKSetupDriver
        {
            readonly IMonitorTestHelper _monitor;
            readonly NormalizedPath _localStoresFolder;

            public Driver( IMonitorTestHelper monitor )
            {
                _monitor = monitor;
                _localStoresFolder = monitor.TestProjectFolder.AppendPart( "TestStores" );
                monitor.OnlyOnce( () => monitor.CleanupFolder( _localStoresFolder ) );
            }

            public NormalizedPath LocalStoresFolder => _localStoresFolder;

            public bool Run( SetupConfiguration configuration, string storePath = null, string remoteStoreUrl = null )
            {
                if( configuration == null ) throw new ArgumentNullException( nameof( configuration ) );
                storePath = Facade.LocateStorePath( _monitor.Monitor, storePath );
                Uri remoteUrl = null;
                if( remoteStoreUrl != "'none'" && remoteStoreUrl != "none" )
                {
                    if( remoteStoreUrl == null ) remoteUrl = Facade.DefaultStoreUrl;
                    else remoteUrl = new Uri( remoteStoreUrl, UriKind.Absolute );
                }

                ClientRemoteStore remote;
                if( remoteUrl != null )
                {
                    remote = new ClientRemoteStore( remoteUrl, null );
                    _monitor.Monitor.Info( $"Using remote '{remoteUrl}'." );
                }
                else
                {
                    remote = null;
                    _monitor.Monitor.Info( $"Not using any remote." );
                }

                using( RuntimeArchive store = RuntimeArchive.OpenOrCreate( _monitor.Monitor, storePath ) )
                {
                    if( store == null ) return false;

                    return Facade.DoRun( _monitor.Monitor,
                                         store,
                                         configuration,
                                         remote );
                }
            }

            public string DotNetPublish( string pathToFramework, bool force = false )
            {
                var publishPath = Path.GetFullPath( Path.Combine( pathToFramework, "publish" ) );
                if( !Directory.Exists( publishPath ) )
                {
                    var framework = Path.GetFileName( pathToFramework );
                    var pathToConfiguration = Path.GetDirectoryName( pathToFramework );
                    var configuration = Path.GetFileName( pathToConfiguration );
                    var projectPath = Path.GetDirectoryName( Path.GetDirectoryName( pathToConfiguration ) );
                    var projectName = Path.GetFileName( projectPath );
                    var pI = new ProcessStartInfo()
                    {
                        WorkingDirectory = projectPath,
                        FileName = "dotnet",
                        Arguments = $"publish -c {configuration} -f {framework}",
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    };
                    using( _monitor.Monitor.OpenInfo( $"Publishing {projectName}: dotnet {pI.Arguments}" ) )
                    using( Process cmdProcess = new Process() )
                    {
                        cmdProcess.StartInfo = pI;
                        cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) _monitor.Monitor.Error( e.Data ); };
                        cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) _monitor.Monitor.Info( e.Data ); };
                        cmdProcess.Start();
                        cmdProcess.BeginErrorReadLine();
                        cmdProcess.BeginOutputReadLine();
                        cmdProcess.WaitForExit();
                        if( cmdProcess.ExitCode != 0 )
                        {
                            _monitor.Monitor.Error( $"Process returned ExitCode {cmdProcess.ExitCode}." );
                            return null;
                        }
                    }
                }
                return publishPath;
            }

            public bool AddComponentFoldersToStore( IEnumerable<string> folders, string storePath = null )
            {
                var monitor = _monitor.Monitor;
                storePath = Facade.LocateStorePath( monitor, storePath );
                var binFolders = Facade.ReadBinFolders( monitor, folders );
                if( binFolders == null ) return false;
                using( RuntimeArchive archive = RuntimeArchive.OpenOrCreate( monitor, storePath ) )
                {
                    return archive != null && archive.CreateLocalImporter().AddComponent( binFolders ).Import();
                }
            }

            public bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey )
            {
                storePath = Facade.LocateStorePath( _monitor.Monitor, storePath );
                using( RuntimeArchive zip = RuntimeArchive.OpenOrCreate( _monitor.Monitor, storePath ) )
                {
                    return zip != null
                            && zip.PushComponents( comp => true, new Uri( remoteStoreUrl, UriKind.Absolute ), apiKey );
                }
            }

            /// <summary>
            /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
            /// Use <see cref="GetCleanTestZipPath"/> to clear it before returning it.
            /// </summary>
            /// <param name="type">Type of the store.</param>
            /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
            /// <param name="name">Name (automatically sets from the caller method name).</param>
            /// <returns>Path to a zip file or a directory that may already exists.</returns>
            public NormalizedPath GetTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null )
            {
                return type == CKSetupStoreType.Zip
                        ? _localStoresFolder.AppendPart( name + suffix + ".zip" )
                        : _localStoresFolder.AppendPart( "FolderStores" ).AppendPart( name + suffix );
            }

            /// <summary>
            /// Gets a path to a local store in <see cref="LocalStoresFolder"/>, either as a zip file or a directory.
            /// If the store exists it is deleted.
            /// </summary>
            /// <param name="type">Type of the store.</param>
            /// <param name="suffix">Optional suffix (will appear before the .zip extension or at the end of the folder).</param>
            /// <param name="name">Name (automatically sets from the caller method name).</param>
            /// <returns>Path to a zip file or directory that does not exist.</returns>
            public NormalizedPath GetCleanTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null )
            {
                var p = GetTestStorePath( type, suffix, name );
                if( type == CKSetupStoreType.Zip ) File.Delete( p );
                else if( Directory.Exists( p ) ) _monitor.CleanupFolder( p );
                return p;
            }
        }

        public CKSetupTestHelper( IMonitorTestHelper monitor )
        {
            _driver = new Driver( monitor );
        }

        ICKSetupDriver ICKSetupTestHelperCore.CKSetup => _driver;

        /// <summary>
        /// Gets the <see cref="ICKSetupTestHelper"/> mixin interface.
        /// </summary>
        public static ICKSetupTestHelper TestHelper => TestHelperResolver.Default.Resolve<ICKSetupTestHelper>();

    }
}
