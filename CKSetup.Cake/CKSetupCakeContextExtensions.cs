using Cake.Core;
using CK.Core;
using System;
using System.Collections.Generic;

namespace CKSetup.Cake
{
    public static class CKSetupCakeContextExtensions
    {
        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core)
        /// into a local store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="Facade.DefaultStorePath"/>.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupAddComponentFoldersToStore(
            this ICakeContext @this,
            IEnumerable<string> folders,
            string storePath = "CodeCakeBuilder/Releases/TempStore" )
        {
            var monitor = new ActivityMonitor();
            var binFolders = Facade.ReadBinFolders( monitor, folders );
            if( binFolders == null ) return false;
            using( RuntimeArchive archive = RuntimeArchive.OpenOrCreate( monitor, storePath ) )
            {
                return archive != null && archive.CreateLocalImporter().AddComponent( binFolders ).Import();
            }
        }

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the remote store.</param>
        /// <param name="storePath">The path to the local store to push.</param>
        /// <param name="remoteStoreUrl">The remote store url. Defaults to <see cref="Facade.DefaultStoreUrl"/>.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool CKSetupPushLocalStoreToRemote(
            string apiKey,
            string storePath = "CodeCakeBuilder/Releases/TempStore",
            string remoteStoreUrl = null )
        {
            var monitor = new ActivityMonitor();
            using( RuntimeArchive zip = RuntimeArchive.OpenOrCreate( monitor, storePath ) )
            {
                return zip != null
                        && zip.PushComponents( comp => true, new Uri( remoteStoreUrl, UriKind.Absolute ), apiKey );
            }
        }


    }
}
