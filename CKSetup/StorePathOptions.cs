using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    class StorePathOptions
    {
        public StorePathOptions( CommandOption arg )
        {
            StorePathOption = arg;
        }

        public CommandOption StorePathOption { get; }

        public string StorePath { get; private set; }
        
        /// <summary>
        /// Initializes this option.
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <param name="binPath">Optional path to look for a store up to the root.</param>
        /// <returns>True on success, false on error.</returns>
        public bool Initialize( IActivityMonitor m, string binPath = null )
        {
            StorePath = Facade.LocateStorePath( m, StorePathOption.Value(), binPath );
            m.Info( $"Using store: {StorePath}" );
            return true;
        }

    }
}
