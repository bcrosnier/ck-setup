using CK.Core;
using System;
using System.Xml.Linq;

namespace StupidEngine
{
    public class Engine
    {
        readonly IActivityMonitor _m;
        readonly XElement _config;

        public Engine( IActivityMonitor m, XElement config )
        {
            _m = m;
            _config = config;
            _m.Info( "Initialized new stupid Engine." );
        }

        public bool Run()
        {
            using( _m.OpenInfo( "Running stupid Engine." ) )
            {
                _m.Info( _config.ToString() );
            }
            return true;
        }

    }
}
