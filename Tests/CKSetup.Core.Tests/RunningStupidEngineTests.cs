using CK.Core;
using CK.Testing;
using CK.Text;
using FluentAssertions;
using NUnit.Framework;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class RunningStupidEngineTests
    {
        [TestCase( "UseRunnerFromCKSetupRemoteSite" )]
        [TestCase( "UseLocalRunner" )]
        public void run_setup_on_StupidModel_net461( string remoteRunner )
        {
            bool localRunner = remoteRunner == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );
            NormalizedPath enginePath = TestHelper.SolutionFolder.Combine( $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/net461" );

            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { enginePath }, storePath )
                .Should().BeTrue();

            if( localRunner )
            {
                NormalizedPath runnerPath = TestHelper.SolutionFolder.Combine( $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/net461" );
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { runnerPath }, storePath )
                    .Should().BeTrue();
            }

            NormalizedPath modelPath = TestHelper.SolutionFolder.Combine( $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/net461" );

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( modelPath );
            conf.Configuration = new XElement("StupidConf");
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            using( WeakAssemblyNameResolver.TemporaryInstall() )
            {
                TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null ).Should().BeTrue();
            }
        }

        [TestCase( "UseRunnerFromCKSetupRemoteSite" )]
        [TestCase( "UseLocalRunner" )]
        public void run_setup_on_StupidModel_netcore( string remoteRunner )
        {
            bool localRunner = remoteRunner == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );
            NormalizedPath enginePath = TestHelper.SolutionFolder.Combine( $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/netcoreapp2.0" );

            string pubEnginePath = TestHelper.CKSetup.DotNetPublish( enginePath, force: true );
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubEnginePath }, storePath )
                .Should().BeTrue();

            if( localRunner )
            {
                NormalizedPath runnerPath = TestHelper.SolutionFolder.Combine( $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/netcoreapp2.0" );
                string pubRunnerPath = TestHelper.CKSetup.DotNetPublish( runnerPath, force: true );
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubRunnerPath }, storePath )
                    .Should().BeTrue();
            }

            NormalizedPath modelPath = TestHelper.SolutionFolder.Combine( $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/netstandard2.0" );

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( modelPath );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            using( WeakAssemblyNameResolver.TemporaryInstall() )
            {
                TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null ).Should().BeTrue();
            }
        }

    }
}
