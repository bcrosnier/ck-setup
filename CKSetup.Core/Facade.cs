using CK.Core;
using CK.Monitoring.InterProcess;
using CK.Text;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// This facade encapsulates the CKSetup run operations.
    /// </summary>
    static public class Facade
    {
        /// <summary>
        /// The default store is the directory Environment.SpecialFolder.LocalApplicationData/CKSetupStore.
        /// </summary>
        public static readonly string DefaultStorePath = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), "CKSetupStore" );

        /// <summary>
        /// The default remote store is http://cksetup.invenietis.net.
        /// </summary>
        public static readonly Uri DefaultStoreUrl = new Uri( "http://cksetup.invenietis.net" );

        /// <summary>
        /// Locates a store that must be used.
        /// This method never fails and ultimately defaults to <see cref="DefaultStorePath"/>.
        /// If a <paramref name="storePath"/> is provided, it will be used (it it does not already exist, a warning is emitted).
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <param name="storePath">A path to an existing store or to a store that will be created.</param>
        /// <param name="startPath">An optional path to look for a store up to the root.</param>
        /// <returns>True on success, false on error.</returns>
        public static string LocateStorePath( IActivityMonitor m, string storePath = null, string startPath = null )
        {
            var result = storePath;
            if( string.IsNullOrEmpty( result ) )
            {
                result = FindStorePathFrom( startPath );
                if( result == null )
                {
                    result = FindStorePathFrom( AppContext.BaseDirectory );
                }
                if( result == null )
                {
                    result = DefaultStorePath;
                }
            }
            else
            {
                result = Path.GetFullPath( result );
                if( !File.Exists( result ) && !Directory.Exists( result ) )
                {
                    m.Warn( $"The provided store '{result}' does not exist. It will be created." );
                }
            }
            return result;
        }

        static string FindStorePathFrom( string dir )
        {
            while( !string.IsNullOrEmpty( dir ) )
            {
                string test = Path.Combine( dir, "CKSetupStore.zip" );
                if( File.Exists( test ) ) return test;
                test = Path.Combine( dir, "CKSetupStore" );
                if( Directory.Exists( test ) ) return test;
                dir = Path.GetDirectoryName( dir );
            }
            return null;
        }

        /// <summary>
        /// Read multiple <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="paths">Folder paths to read.</param>
        /// <returns>The list of bin folders or null if an error occurred.</returns>
        public static List<BinFolder> ReadBinFolders( IActivityMonitor monitor, IEnumerable<string> paths )
        {
            var result = new List<BinFolder>();
            using( monitor.OpenDebug( "Discovering files." ) )
            {
                try
                {
                    foreach( var d in paths )
                    {
                        var f = BinFolder.ReadBinFolder( monitor, d );
                        if( f == null ) return null;
                        result.Add( f );
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                    return null;
                }
            }
            if( result.Count == 0 )
            {
                monitor.Error( "No valid runtime files found." );
                return null;
            }
            return result;
        }

        /// <summary>
        /// Main entry point to CKSetup run.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="archive">The opened, valid, runtime archive.</param>
        /// <param name="config">The setup configuration.</param>
        /// <param name="missingImporter">Optional component importer.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool DoRun(
            IActivityMonitor monitor,
            RuntimeArchive archive,
            SetupConfiguration config,
            IComponentImporter missingImporter = null )
        {
            using( monitor.OpenTrace( "Running Setup." ) )
            {
                string workingDir = null;
                try
                {
                    IReadOnlyDictionary<string, BinFileInfo> dedupFiles;
                    IReadOnlyList<BinFolder> folders = ReadBinFolders( monitor, config.BinPaths, out dedupFiles );
                    if( folders == null ) return false;
                    workingDir = GetWorkingDirectory( monitor, config.WorkingDirectory, folders );
                    if( workingDir == null ) return false;

                    IEnumerable<SetupDependency> manualDependencies = config.Dependencies;

                    var thisAssembly = (AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                    var thisVersion = new InformationalVersion( thisAssembly?.InformationalVersion );
                    if( thisVersion.SemVersion != null && thisVersion.SemVersion != SVersion.ZeroVersion )
                    {
                        manualDependencies = manualDependencies.Append( new SetupDependency( "CKSetup.Runner", SVersion.Parse( "5.0.0-d06-00-develop-0026" ) ) );
                    }
                    else
                    {
                        manualDependencies = manualDependencies.Append( new SetupDependency( "CKSetup.Runner", SVersion.ZeroVersion ) );
                    }
                    if( !archive.ExtractRuntimeDependencies( workingDir, folders, missingImporter, manualDependencies ) ) return false;

                    using( monitor.OpenInfo( $"Copying {dedupFiles.Count} files from bin folders." ) )
                    {
                        foreach( var f in dedupFiles.Values )
                        {
                            string targetPath = workingDir + f.LocalFileName;
                            if( File.Exists( targetPath ) )
                            {
                                monitor.Trace( $"Skipping already exisiting '{f.LocalFileName}'." );
                            }
                            else
                            {
                                File.Copy( f.FullPath, targetPath );
                            }
                        }
                    }
                    using( monitor.OpenInfo( $"Generating CKSetup.Runner.Config.xml file." ) )
                    {
                        var root = new XElement( config.Configuration );
                        var ckSetup = new XElement( SetupConfiguration.xCKSetup );
                        ckSetup.Add( new XElement( SetupConfiguration.xEngineAssemblyQualifiedName, config.EngineAssemblyQualifiedName ) );
                        var binPaths = new XElement( SetupConfiguration.xBinPaths );
                        foreach( BinFolder f in folders )
                        {
                            var binPath = new XElement( SetupConfiguration.xBinPath, new XAttribute( SetupConfiguration.xBinPath, f.BinPath ) );
                            var models = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.Model );
                            var dependencies = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.SetupDependency );
                            var modelDependents = f.Assemblies.Where( a => a.ComponentKind == ComponentKind.None && a.LocalDependencies.Any( dep => dep.ComponentKind == ComponentKind.Model ) );
                            binPath.Add( models.Select( a => new XElement( SetupConfiguration.xModel, a.Name.Name ) ) );
                            binPath.Add( dependencies.Select( a => new XElement( SetupConfiguration.xDependency, a.Name.Name ) ) );
                            binPath.Add( modelDependents.Select( a => new XElement( SetupConfiguration.xModelDependent, a.Name.Name ) ) );
                            binPaths.Add( binPath );
                        }
                        ckSetup.Add( binPaths );
                        root.Add( ckSetup );
                        monitor.Debug( root.ToString() );
                        new XDocument( root ).Save( Path.Combine( workingDir, "CKSetup.Runner.Config.xml" ) );
                    }
                    return RunSetupRunner( monitor, workingDir );
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                    return false;
                }
                finally
                {
                    if( workingDir != null && workingDir.StartsWith( Path.GetTempPath() ) )
                    {
                        DeleteTemporaryWorkingFolder( monitor, workingDir );
                    }
                }
            }
        }

        static IReadOnlyList<BinFolder> ReadBinFolders(
            IActivityMonitor monitor,
            IEnumerable<string> binPaths,
            out IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            dedupFiles = null;
            using( monitor.OpenInfo( "Reading binary folders." ) )
            {
                var folders = binPaths.Select( p => BinFolder.ReadBinFolder( monitor, p ) ).ToList();
                if( folders.Any( f => f == null ) ) return null;
                if( folders.Count == 0 )
                {
                    monitor.Error( $"No BinPath provided. At least one is required." );
                    return null;
                }
                using( monitor.OpenInfo( "Checking that common files are the same." ) )
                {
                    bool conflict = false;
                    Dictionary<string, BinFileInfo> byName = new Dictionary<string, BinFileInfo>();
                    foreach( var folder in folders )
                    {
                        foreach( var file in folder.Files )
                        {
                            if( byName.TryGetValue( file.LocalFileName, out var exists ) )
                            {
                                if( exists.ContentSHA1 != file.ContentSHA1 )
                                {
                                    conflict = true;
                                    using( monitor.OpenError( $"File mismatch: {file.LocalFileName}" ) )
                                    {
                                        monitor.Info( $"{exists.BinFolder.BinPath} has {exists.ToString()} with SHA1: {exists.ContentSHA1}." );
                                        monitor.Info( $"{file.BinFolder.BinPath} has {file.ToString()} with SHA1: {file.ContentSHA1}." );
                                    }
                                }
                            }
                            else byName.Add( file.LocalFileName, file );
                        }
                    }
                    if( conflict ) return null;
                    dedupFiles = byName;
                }
                monitor.CloseGroup( $"{folders.Count} folders read." );
                return folders;
            }
        }

        static string GetWorkingDirectory( IActivityMonitor monitor, string workingDir, IReadOnlyList<BinFolder> folders )
        {
            if( !String.IsNullOrWhiteSpace( workingDir ) )
            {
                workingDir = FileUtil.NormalizePathSeparator( Path.GetFullPath( workingDir ), true );
                if( folders.Any( b => b.BinPath.StartsWith( workingDir, StringComparison.OrdinalIgnoreCase ) ) )
                {
                    monitor.Error( $"Working directory can not be inside one of the bin paths." );
                    return null;
                }
                if( Directory.Exists( workingDir ) )
                {
                    monitor.Info( $"Clearing provided working directory: {workingDir}." );
                    Directory.Delete( workingDir, true );
                }
                else
                {
                    monitor.Info( $"Creating provided working directory: {workingDir}." );
                }
            }
            else
            {
                workingDir = $"{Path.GetTempPath()}CKSetup{Path.DirectorySeparatorChar}{Guid.NewGuid().ToString( "N" )}{Path.DirectorySeparatorChar}";
                monitor.Info( $"Created temporary working directory: {workingDir}." );
            }
            Directory.CreateDirectory( workingDir );
            return workingDir;
        }

        static void DeleteTemporaryWorkingFolder( IActivityMonitor monitor, string workingDir )
        {
            using( monitor.OpenInfo( $"Deleting temporary working directory." ) )
            {
                int retryCount = 0;
                for(; ; )
                {
                    try
                    {
                        Directory.Delete( workingDir, true );
                        break;
                    }
                    catch( Exception ex )
                    {
                        if( ++retryCount < 3 )
                        {
                            monitor.Warn( $"Error while deleting folder. Retrying (count = {retryCount}).", ex );
                            System.Threading.Thread.Sleep( 500 );
                        }
                        else
                        {
                            monitor.Error( $"Unable to delete folder '{workingDir}'.", ex );
                            break;
                        }
                    }
                }
            }
        }

        static bool RunSetupRunner(
            IActivityMonitor m,
            string workingDir )
        {
            using( m.OpenInfo( "Launching CKSetup.Runner process." ) )
            {
                string exe = Path.Combine( workingDir, "CKSetup.Runner.exe" );
                string dll = Path.Combine( workingDir, "CKSetup.Runner.dll" );
                string fileName, arguments;
                if( !File.Exists( exe ) )
                {
                    if( !File.Exists( dll ) )
                    {
                        m.Error( "Unable to find CKSetup.Runner in folder." );
                        return false;
                    }
                    fileName = "dotnet";
                    #region Using existing runtimeconfig.json to create CKSetup.Runner.runtimeconfig.json.
                    {
                        const string runnerConfigFileName = "CKSetup.Runner.runtimeconfig.json";
                        // We try to find an existing runtimeconfig.json:
                        // - First look for unique runtimeconfig.dev.json
                        //    - It must be unique and have an associated runtimeconfig.json file otherwise we fail.
                        //    - If no dev file exists, we look for a unique runtimeconfig.json
                        // - If there is runtimeconfig.json duplicates in the folder, we fail.
                        // - If there is no runtimeconfig.json, we generate a default one.
                        if( !FindRuntimeConfigFiles( m, workingDir, out string fRtDevPath, out string fRtPath ) )
                        {
                            return false;
                        }
                        string runnerFile = Path.Combine( workingDir, runnerConfigFileName );
                        // If there is a dev file, extracts its additional probe paths.
                        string additionalProbePaths = ExtractJsonAdditonalProbePaths( m, fRtDevPath );
                        if( additionalProbePaths == null )
                        {
                            if( fRtPath == null )
                            {
                                const string defaultRt = "{\"runtimeOptions\":{\"tfm\":\"netcoreapp2.0\",\"framework\":{\"name\":\"Microsoft.NETCore.App\",\"version\": \"2.0.0\"}}}";
                                m.Info( $"Trying with a default {runnerConfigFileName}: {defaultRt}" );
                                File.WriteAllText( runnerFile, defaultRt );
                            }
                            else File.Copy( fRtPath, runnerFile, true );
                        }
                        else
                        {
                            string txt = File.ReadAllText( fRtPath );
                            int idx = txt.LastIndexOf( '}' ) - 1;
                            if( idx > 0 ) idx = txt.LastIndexOf( '}', idx ) - 1;
                            if( idx > 0 )
                            {
                                txt = txt.Insert( idx, "," + additionalProbePaths );
                                File.WriteAllText( runnerFile, txt );
                            }
                            else
                            {
                                using( m.OpenError( "Unable to inject additionalProbingPaths in:" ) )
                                {
                                    m.Error( txt );
                                }
                                return false;
                            }
                        }
                    }
                    #endregion
                    #region Merging all deps.json into CKSetup.Runner.deps.json
                    {
                        arguments = "CKSetup.Runner.dll merge-deps";
                        if( !RunCKSetupRunnerProcess( m, workingDir, fileName, arguments ) )
                        {
                            return false;
                        }
                        string theFile = Path.Combine( workingDir, "CKSetup.Runner.deps.json" );
                        string theBackup = theFile + ".original";
                        File.Replace( theFile + ".merged", theFile, theBackup );
                    }
                    #endregion
                    arguments = "CKSetup.Runner.dll ";
                }
                else
                {
                    fileName = exe;
                    arguments = String.Empty;
                }
                return RunCKSetupRunnerProcess( m, workingDir, fileName, arguments );
            }
        }

        static bool RunCKSetupRunnerProcess(
                IActivityMonitor m,
                string workingDir,
                string fileName,
                string arguments )
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDir,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = fileName
            };
            using( var logReceiver = SimpleLogPipeReceiver.Start( m, true ) )
            {
                cmdStartInfo.Arguments = arguments;
                cmdStartInfo.Arguments += " /logPipe:" + logReceiver.PipeName;
                using( m.OpenTrace( $"{fileName} {cmdStartInfo.Arguments}" ) )
                using( Process cmdProcess = new Process() )
                {
                    cmdProcess.StartInfo = cmdStartInfo;
                    cmdProcess.Start();
                    cmdProcess.WaitForExit();
                    var endLogStatus = logReceiver.WaitEnd( cmdProcess.ExitCode != 0 );
                    if( endLogStatus != LogReceiverEndStatus.Normal )
                    {
                        m.Warn( $"Pipe log channel abnormal end status: {endLogStatus}." );
                    }
                    if( cmdProcess.ExitCode != 0 )
                    {
                        m.Error( $"Process returned ExitCode {cmdProcess.ExitCode}." );
                        return false;
                    }
                    return true;
                }
            }
        }

        static string ExtractJsonAdditonalProbePaths( IActivityMonitor m, string fRtDevPath )
        {
            if( fRtDevPath == null ) return null;
            string txt = File.ReadAllText( fRtDevPath );
            var matcher = new StringMatcher( txt );
            List<KeyValuePair<string, object>> oRoot;
            int idxRuntimeOptions;
            List<KeyValuePair<string, object>> oRuntimeOptions;
            int idxAdditionalProbingPaths;
            List<object> additionalProbingPathsObj;
            if( matcher.MatchJSONObject( out object oConfig )
                && (oRoot = (oConfig as List<KeyValuePair<string, object>>)) != null
                && (idxRuntimeOptions = oRoot.IndexOf( kv => kv.Key == "runtimeOptions" )) >= 0
                && (oRuntimeOptions = oRoot[idxRuntimeOptions].Value as List<KeyValuePair<string, object>>) != null
                && (idxAdditionalProbingPaths = oRuntimeOptions.IndexOf( kv => kv.Key == "additionalProbingPaths" )) >= 0
                && (additionalProbingPathsObj = oRuntimeOptions[idxAdditionalProbingPaths].Value as List<object>) != null
                && additionalProbingPathsObj.All( p => p is string ) )
            {
                m.Trace( $"Extracted {additionalProbingPathsObj.OfType<string>().Concatenate()} from {fRtDevPath}." );
                return "\"additionalProbingPaths\":[\""
                            + additionalProbingPathsObj
                                .Select( p => ((string)p).Replace("\\","\\\\" ) )
                                .Concatenate( "\", \"" )
                            + "\"]";
            }
            using( m.OpenWarn( "Unable to extract any additional probing paths from:" ) )
            {
                m.Warn( txt );
            }
            return null;
        }

        private static bool FindRuntimeConfigFiles( IActivityMonitor m, string binPath, out string fRtDevPath, out string fRtPath )
        {
            fRtDevPath = fRtPath = null;
            // First find the dev file.
            var devs = Directory.EnumerateFiles( binPath, "*.runtimeconfig.dev.json" ).ToList();
            if( devs.Count > 1 )
            {
                m.Error( $"Found more than one runtimeconfig.dev.json files: '{String.Join( "', '", devs.Select( p => Path.GetFileName( p ) ) ) }'." );
                return false;
            }
            if( devs.Count == 1 )
            {
                fRtDevPath = devs[0];
                string fRtDevName = Path.GetFileName( fRtDevPath );
                m.Trace( $"Found '{fRtDevName}'." );
                string fRtName = fRtDevName.Remove( fRtDevName.Length - 9, 4 );
                fRtPath = Path.Combine( binPath, fRtName );
                if( !File.Exists( fRtPath ) )
                {
                    m.Error( $"Unable to find '{fRtName}' file (but found '{fRtDevName}')." );
                    return false;
                }
            }
            else
            {
                var rtFiles = Directory.EnumerateFiles( binPath, "*.runtimeconfig.json" ).ToList();
                if( rtFiles.Count > 1 )
                {
                    m.Error( $"Found more than one runtimeconfig.json files: '{String.Join( "', '", rtFiles.Select( p => Path.GetFileName( p ) ) ) }'." );
                    return false;
                }
                else if( rtFiles.Count == 0 )
                {
                    // When there is NO runtimeconfig.json file, this is not an error:
                    // We'll use a default one (and pray).
                    m.Warn( $"Unable to find a runtimeconfig.json file." );
                    return true;
                }
                fRtPath = rtFiles[0];
            }
            return true;
        }

    }
}
