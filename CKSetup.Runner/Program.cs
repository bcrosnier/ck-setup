using System;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Text;
using CK.Core;
using System.Collections.Generic;

namespace CKSetup.Runner
{
    public static partial class Program
    {
        static public int Main( string[] args )
        {
            // See https://github.com/dotnet/corefx/issues/23608
            CultureInfo.CurrentCulture
                = CultureInfo.CurrentUICulture
                = CultureInfo.DefaultThreadCurrentCulture
                = CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo( "en-US" );

            string rawLogPath = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.RawLogs.txt" );
            if( File.Exists( rawLogPath ) ) File.Delete( rawLogPath );
            var rawLogText = new StringBuilder();

            IReadOnlyList<AssemblyLoadConflict> conflicts = null;
            try
            {
                using( var w = WeakAssemblyNameResolver.TemporaryInstall( c => conflicts = c ) )
                {
                    return ActualRunner.Run( rawLogText, args, () => w.Conflicts );
                }
            }
            catch( Exception ex )
            {
                rawLogText.Append( "Exception: " ).AppendLine( ex.Message );
                while( ex.InnerException != null )
                {
                    ex = ex.InnerException;
                    rawLogText.Append( " => Inner: " ).AppendLine( ex.Message );
                }
                return 1;
            }
            finally
            {
                if( conflicts != null && conflicts.Count > 0 )
                {
                    rawLogText.AppendLine( $"{conflicts.Count} assembly load conflicts:" );
                    foreach( var c in conflicts )
                    {
                        rawLogText.AppendLine( c.ToString() );
                    }
                }
                File.WriteAllText( rawLogPath, rawLogText.ToString() );
            }
        }

    }
}
